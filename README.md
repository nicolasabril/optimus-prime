# optimus-prime
O grande líder Optimus -- Processador baseado em PIC feito em VHDL



----------------

### Diretórios e arquivos
- `src` contém os arquivos fonte vhd
- `testes` contém os testbenches para os componentes
- `build` contém os arquivos gerados pelo ghdl
- `compila-componentes.sh` é um script que compila os arquivos de `src`
- `LICENSE` contém informações sobre a licensa do projeto
----------------

### Autores
- Nicolas Abril
- Lucca Rawlyk Holosbach

[Link para o repositório](https://gitlab.com/nicolasabril/optimus-prime)
